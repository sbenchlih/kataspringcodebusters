package com.codebusters.dataproviderservice.controller;

import com.codebusters.dataproviderservice.dto.ForexResponse;
import com.codebusters.dataproviderservice.service.ForexService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/forex")
@RequiredArgsConstructor
public class ForexController {
    private final ForexService forexService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ForexResponse> getAllForex(){
        return forexService.getAllForex();
    }
}
