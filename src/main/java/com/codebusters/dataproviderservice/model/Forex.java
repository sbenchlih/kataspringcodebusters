package com.codebusters.dataproviderservice.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "forex")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Forex {
    @Id
    private Long id;
    private String currency_base;
    private String currency_target;
    private double worth;
}
