package com.codebusters.dataproviderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ForexResponse {
    private Long id;
    private String currency_base;
    private String currency_target;
    private double value;

}
