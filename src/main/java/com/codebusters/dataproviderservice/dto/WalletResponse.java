package com.codebusters.dataproviderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class WalletResponse {
    private Long id;
    private String portfolio;
    private String product;
    private String underlying;
    private String currency;
    private int price;
}
