package com.codebusters.dataproviderservice.repository;

import com.codebusters.dataproviderservice.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletRepository extends JpaRepository<Wallet, Long> {
}
