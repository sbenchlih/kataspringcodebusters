package com.codebusters.dataproviderservice.repository;

import com.codebusters.dataproviderservice.model.Forex;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForexRepository extends JpaRepository<Forex, Long> {

}
