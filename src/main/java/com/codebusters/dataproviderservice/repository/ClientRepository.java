package com.codebusters.dataproviderservice.repository;

import com.codebusters.dataproviderservice.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
