package com.codebusters.dataproviderservice.service;

import com.codebusters.dataproviderservice.dto.ForexResponse;
import com.codebusters.dataproviderservice.model.Forex;
import com.codebusters.dataproviderservice.repository.ForexRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class ForexService {

    private final ForexRepository forexRepository;

    public List<ForexResponse> getAllForex() {
        List<Forex> forexes = forexRepository.findAll();
        return forexes.stream().map(this::mapToForexResponse).toList();
    }

    private ForexResponse mapToForexResponse(Forex forex) {
        return ForexResponse.builder()
                .id(forex.getId())
                .currency_base(forex.getCurrency_base())
                .currency_target(forex.getCurrency_target())
                .value(forex.getWorth())
                .build();

    }
}
